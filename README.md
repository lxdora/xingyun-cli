<h1>xingyun-cli</h1>

这是一个低代码脚手架，用于生成一套固定的页面模板，然后根据配置文件生成页面

<h1>Installation</h1>

```npm install -g xingyun-cli```

<h1>How to use</h1>

`xingyun create name` 创建name项目并拉取页面模板 
`cd name`  进入项目目录 

`xingyun update` 根据配置文件生成页面
