// lib/Generator.js

const util = require('util')
const path = require('path')
const downloadGitRepo = require('download-git-repo') // 不支持 Promise
const ora = require('ora')

// 添加加载动画
async function wrapLoading(fn, message, ...args) {
  // 使用 ora 初始化，传入提示信息 message
  const spinner = ora(message);
  // 开始加载动画
  spinner.start();

  try {
    // 执行传入方法 fn
    const result = await fn(...args);
    // 状态为修改为成功
    spinner.succeed();
    return result; 
  } catch (error) {
    // 状态为修改为失败
    spinner.fail(`Request failed, ${error}`)
  } 
}

class InitProject {
  projectUrl = ''
  requestUrl = ''
  constructor (projectUrl, requestUrl){
    this.projectUrl = projectUrl
    this.requestUrl = requestUrl
    // 对 download-git-repo 进行 promise 化改造
    this.downloadGitRepo = util.promisify(downloadGitRepo)
  }
  // 下载远程模板
  // 1）拼接下载地址
  // 2）调用下载方法
  async download(){
    // 2）调用下载方法
    await wrapLoading(
      this.downloadGitRepo, // 远程下载方法
      'waiting download template ', // 加载提示信息
      this.requestUrl, // 参数1: 下载地址
      this.projectUrl,
      {clone: true})
  }

  async create(){
    // 3）下载模板到模板目录
    await this.download()
  }
}

module.exports = InitProject;
