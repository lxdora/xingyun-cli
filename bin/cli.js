#! /usr/bin/env node

const program = require('commander')
const path = require('path')
const mkdirp = require('mkdirp')
const figlet = require('figlet')
const inquirer = require('inquirer')
const cwdUrl = process.cwd()

program
.version('1.0.0')
.command('create <name>')
.description('create a new project')
.action(async name => { 
  //新建项目目录
  const projectUrl = path.join(cwdUrl, name)
  mkdirp(projectUrl)
  const InitProject = require('./InitProject')
  // const requestUrl = "direct:https://gitlab.com/lxdora/low_code.git"
  const requestUrl = "direct:https://git.hoge.cn/microfed/low_code.git"
  await new InitProject(projectUrl, requestUrl).create()
  const renderTemplate = require('./render')
  inquirer.prompt([
    {
      type: 'input',
      name: 'appName',
      message: '请输入服务名称',
    }
  ]).then(answer=>{
    renderTemplate.render(name, answer.appName)
  })
})

program
  .command('update')
  .description('Update your project view according to the configuration file')
  .action(() => {
    const update = require('./update')
    update.generate()
  })

  program
  .on('--help', () => {
    // 使用 figlet 绘制 Logo
    console.log('\r\n' + figlet.textSync('xingyun', {
      horizontalLayout: 'full',
      verticalLayout: 'default',
      width: 100,
      whitespaceBreak: true
    }));
  })

program.parse()