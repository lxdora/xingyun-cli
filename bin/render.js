// 渲染 index.html文件

const fs = require('fs')
const path = require('path')
const cwdUrl = process.cwd()

async function readFile(file){
  return new Promise((resolve, reject)=>{
    fs.readFile(file, (err, data)=>{
      if(err){
        reject(err)
      }
      resolve(data.toString())
    })
  })
}

exports.render = async function(name, appName){
  const htmlPath = path.join(cwdUrl, name + '/public/index.html')
  //读取文件
  const str = await readFile(htmlPath)
  const headerPath = path.join(cwdUrl, name + '/src/components/DefaultHeader.vue')
  const headerContent = await readFile(headerPath)
  try{
    fs.writeFileSync(htmlPath, str.replace("Document", name))
    //替换头部名称
    fs.writeFileSync(headerPath, headerContent.replace("appName", appName))
    console.log('渲染模板成功');
  }catch(err){
    console.log('渲染模板失败', err);
  }
}