//根据配置文件生成文件
const path = require('path')
const fs = require('fs')
const mkdirp = require('mkdirp')
const walker = require('walker')
const inquirer = require('inquirer')
const cwd = process.cwd()
const del = require('del')
const shell = require('shelljs')

async function walkDir(dir){
  const confTemp = 'public/properties'
  const viewTemp = 'src/views'
  walker(dir).on('dir', async function(dir, stat) {
    const viewDirPath = dir.replace(confTemp, viewTemp)
    mkdirp(viewDirPath)
  }).on('file', async function(file, stat) {
    // const data = await readConfigurationFile(file)
    const config = require(file)
    const viewFilePath = file.replace(confTemp, viewTemp).replace('js', 'vue')
    await generateViewByConfig(config.config, file, viewFilePath)
  })
}

async function readConfigurationFile(file) {
  return new Promise((resolve, reject)=>{
    fs.readFile(file, (err, data)=>{
      if(err){
        reject(err)
      }
      resolve(data.toString())
    })
  })
}

async function generateViewByConfig(conf, jsFilePath, viewPath) {
  let templatePath = ''
  if(conf['type']==='menu'){
    return true
  }
  switch(conf["type"]){
    case "table":
      templatePath = path.join(cwd, "template/TableTemplate.vue")
      break
    case "form":
      templatePath = path.join(cwd, 'template/FormTemplate.vue')
      break
    case "Timeline":
      templatePath = path.join(cwd, 'template/TimelineTemplate.vue')
      break
    default:
      console.log(`${conf["type"]}类型模板正在开发中，敬请期待`)
      return 
  }
  fs.readFile(templatePath, (err, data)=>{
    if(err){
      console.log(err);
    }
    const jsPath = jsFilePath.split('/properties/')[1]
    let fileName = ''
    if(jsPath.includes('/')){
      fileName = jsPath.split('/')[1].replace('.js', '')
    }else{
      fileName = jsPath.replace('js', '')
    }
    if(data){
      const handleData = data.toString().replace(/ConfigPath/g, jsPath).replace(/DefaultTemplate/g, fileName)
      if(fs.existsSync(viewPath)){
        fs.writeFile(viewPath, handleData, (err)=>{
          if(err) throw err
          console.log(`更新文件${viewPath}成功`)
        })
      }else{
        console.log(`文件${viewPath}不存在`);
      }
    }
  })
}

async function readConfPath(confPath) {
  walkDir(confPath)
  // 执行gitsubmodule
  const libraryPath = path.join(cwd, 'src/library')
  if (!shell.which('git')) {
    shell.echo('加载子模块需要使用git,您的环境中没有git, 退出');
    shell.exit(1);
  }
  const {code, output} =  shell.exec('git init')
  if(code!=0){
    console.log(output);
  }
  shell.exec('git submodule add git@git.hoge.cn:microfed/library.git src/library')
}

exports.generate = async function () {
  // 配置文件目录
  const confPath = path.join(cwd, 'public/properties')
  //视图目录
  const viewPath = path.join(cwd, 'src/views')
  mkdirp(viewPath).then(()=>{
    //判断视图是否为空， 如不为空，询问是否清空
    const files = fs.readdirSync(viewPath)
    if(files.length!=0){
      inquirer.prompt([{
        type: 'confirm',
        name: 'confirm',
        message: `视图目录${viewPath}不为空，是否清空目录？`
      }]).then(answer=>{
        if(answer.confirm){
          del(viewPath+'/**')
        }
        readConfPath(confPath, viewPath)
      })
    }else{
      readConfPath(confPath, viewPath)
    }
  })
}


